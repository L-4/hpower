print("Hello from Hpower!")

local log = hutil.make_logger("Hpower")

minetest.register_node("hpower:generator", {
    tiles = {
        "hpower_na_topbottom.png", "hpower_na_topbottom.png",
        "hpower_na_side.png", "hpower_na_side.png", "hpower_na_side.png",
        "hpower_generator_front.png"
    },
    description = "Generator",
    groups = {oddly_breakable_by_hand = 1, cracky = 1},
    on_construct = function(pos)
        logistics.power_io.create(pos, 100000, 0, 63)
    end,
    on_rightclick = function(pos, node, clicker)
        local power_io = logistics.power_io.get(pos)

        log("info", "Block power %d", power_io:get_current_power())
    end
})

logistics.power_io.register_power_block("hpower:generator", {})

minetest.register_node("hpower:battery", {
    tiles = {
        "hpower_na_topbottom.png", "hpower_na_topbottom.png",
        "hpower_na_side.png", "hpower_na_side.png", "hpower_na_side.png",
        "hpower_battery_front.png"
    },
    description = "Battery",
    groups = {oddly_breakable_by_hand = 1, cracky = 1},
    on_construct = function(pos) logistics.power_io.create(pos, 100000, 0, 0) end,
    on_rightclick = function(pos, node, clicker)
        local power_io = logistics.power_io.get(pos)

        power_io:change_current_power(100)
    end
})

logistics.power_io.register_power_block("hpower:battery", {
    on_power_change = function(pos, delta, new_total)
        log("info", "Power level changed by %d, now %d", delta, new_total)
    end
})

minetest.register_node("hpower:sink", {
    tiles = {
        "hpower_na_topbottom.png", "hpower_na_topbottom.png",
        "hpower_na_side.png", "hpower_na_side.png", "hpower_na_side.png",
        "hpower_sink_front.png"
    },
    description = "Sink",
    groups = {oddly_breakable_by_hand = 1, cracky = 1},
    on_construct = function(pos)
        logistics.power_io.create(pos, 100000, 63, 0)
    end,
    on_rightclick = function(pos, node, clicker)
        local power_io = logistics.power_io.get(pos)

        log("info", "Block power %d", power_io:get_current_power())
    end
})

logistics.power_io.register_power_block("hpower:sink", {})

--- @todo: This needs to be moved into the network file.
minetest.register_node("hpower:cable", {
    tiles = {"hpower_cable.png"},
    description = "Cable",
    groups = {oddly_breakable_by_hand = 1, cracky = 1},
    on_construct = function(pos) logistics.power_network.construct_cable(pos) end,
    on_destruct = function(pos) logistics.power_network.destruct_cable(pos) end,
    on_rightclick = function(pos, node, clicker)
        logistics.power_network.assert(pos)
    end
})

logistics.power_network.register_power_cable("hpower:cable", {max_io = 8000})
